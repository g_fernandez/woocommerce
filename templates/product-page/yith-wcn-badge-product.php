<?php
$product_settings = get_post_meta( $product_id );
?>
<span class="soldout_product badge_product_<?php echo esc_attr( $product_id ); ?>">
	<?php echo esc_attr( $product_settings['yith_wcn_badge_text'][0] ); ?>
</span>
