<?php
$product_settings = get_post_meta( $product_id );
?>
<div class="note-container">
	<h3><?php echo esc_attr( $product_settings['yith_wcn_note_label'][0] ); ?></h3>

	<p><?php echo esc_attr( $product_settings['yith_wcn_note_description'][0] ); ?></p>

	<?php
	if ( 'text' === $product_settings['yith_wcn_field_type'][0] ){
		?>
		<input type="text" name="yith_wcn_purchase_note_text_field" id="yith_wcn_purchase_note_text_field" data-product_id="<?php echo esc_attr( $product_id ); ?>">
		<?php
	} else if ( 'textarea' === $product_settings['yith_wcn_field_type'][0] ){
		?>
		<textarea name="yith_wcn_purchase_note_text_field" id="yith_wcn_purchase_note_text_field" data-product_id="<?php echo esc_attr( $product_id ); ?>"></textarea>
		<?php
	}
	?>

	<?php
	if ( 'free' === $product_settings['yith_wcn_price_settings'][0] ){
		?>
		<p class="yith_price">Free</p>
		<?php
	} elseif ( 'fixed_price' === $product_settings['yith_wcn_price_settings'][0] ) {
		?>
		<p class="yith_price">+ <?php echo esc_attr( $product_settings['yith_wcn_price'][0] ); ?> <span><?php echo esc_attr( get_woocommerce_currency_symbol() ); ?></span></p>
		<?php
	} elseif ( 'price_per_character' === $product_settings['yith_wcn_price_settings'][0] ) {
		?>
		<p><?php echo esc_attr__( 'Price per character: ', 'yith-plugin-notes' ) . esc_attr( $product_settings['yith_wcn_price'][0] ); ?><span class="woocommerce-Price-currencySymbol"><?php echo esc_attr( get_woocommerce_currency_symbol() ); ?></span></p>
		<p class="yith_price"></p>
		<?php
	}
	?>

</div>
