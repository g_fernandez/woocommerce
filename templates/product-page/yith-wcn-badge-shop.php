<?php
$product_settings = get_post_meta( $product_id );
?>
<style>
	.badge_shop_<?php echo esc_attr( $product_id ); ?>{
		background-color: <?php echo esc_attr( $product_settings['yith_wcn_badge_background_color'][0] ); ?>;
		color: <?php echo esc_attr( $product_settings['yith_wcn_badge_text_color'][0] ); ?>;
	}
</style>
<span class="soldout_shop badge_shop_<?php echo esc_attr( $product_id ); ?>">
	<?php echo esc_attr( $product_settings['yith_wcn_badge_text'][0] ); ?>
</span>
