<?php
/**
 * Plugin Name: YITH Plugin Notes
 * Descriplion: Notes for YITH Plugins
 * Version: 1.0.0
 * Author: German Fernandez
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-notes
 *
 * @package plugin-wc
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'YITH_WCN_VERSION' ) ) {
	define( 'YITH_WCN_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_WCN_DIR_URL' ) ) {
	define( 'YITH_WCN_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_WCN_DIR_ASSETS_URL' ) ) {
	define( 'YITH_WCN_DIR_ASSETS_URL', YITH_WCN_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_WCN_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_WCN_DIR_ASSETS_CSS_URL', YITH_WCN_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_WCN_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_WCN_DIR_ASSETS_JS_URL', YITH_WCN_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_WCN_DIR_PATH' ) ) {
	define( 'YITH_WCN_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_WCN_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_WCN_DIR_INCLUDES_PATH', YITH_WCN_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_WCN_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_WCN_DIR_TEMPLATES_PATH', YITH_WCN_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_WCN_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_WCN_DIR_VIEWS_PATH', YITH_WCN_DIR_PATH . '/views' );
}


if ( ! function_exists( 'yith_wcn_init_classes' ) ) {

	/**
	 * Yith_wcn_init_classes
	 *
	 * @return void
	 */
	function yith_wcn_init_classes() {

		$i = load_plugin_textdomain( 'yith-plugin-notes', false, basename( dirname( __FILE__ ) ) . '/languages' );
		require_once YITH_WCN_DIR_INCLUDES_PATH . '/class-yith-wcn-plugin-notes.php';
		if ( class_exists( 'YITH_WCN_Plugin_Notes' ) ) {
			yith_wcn_plugin_notes();
		}
	}
}

add_action( 'plugins_loaded', 'yith_wcn_init_classes' );
