jQuery( document ).ready( function() {

  jQuery("#yith_wcn_purchase_note_text_field").on('input',function(e){
    product_id = jQuery(this).attr("data-product_id");
    input_val = jQuery(this).val();

    jQuery.ajax({
      type: "POST",
      dataType: "json",
      url: myAjax.ajaxurl,
      data: {
        action: "change_note_price",
        symbol: '',
        price: 0,
        product_id: product_id,
        input_val: input_val,
      },
      success: function (response) {
        if ( 0 === response.price ){
          jQuery(".yith_price").html('Free');
        } else {
          jQuery(".yith_price").html('+ '+response.price+' '+response.symbol);
        }
      }
    })

  })

})
