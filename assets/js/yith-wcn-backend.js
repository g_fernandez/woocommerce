jQuery( document ).ready(function() {

  jQuery( '.my-color-field' ).wpColorPicker();

  if ( jQuery( '#yith_wcn_enable_note' ).prop( 'checked' ) ) {
    jQuery( '.yith_wcn_note_label_input' ).show();
    jQuery( '.yith_wcn_note_description_input' ).show();
    jQuery( '.yith_wcn_field_type_input' ).show();
    jQuery( '.yith_wcn_price_settings_input' ).show();
    if ( jQuery( '#free' ).prop( 'checked' ) ) {
      jQuery( '.yith_wcn_price_input' ).hide();
      jQuery( '.yith_wcn_free_characters_input' ).hide();
    }else{
      jQuery( '.yith_wcn_price_input' ).show();
      jQuery( '.yith_wcn_free_characters_input' ).show();
    }
    jQuery( '.yith_wcn_show_badge_input' ).show();
    if ( jQuery( '#yith_wcn_show_badge' ).prop( 'checked' ) ) {
      jQuery( '.yith_wcn_badge_text_input' ).show();
      jQuery( '.yith_wcn_badge_background_color_input' ).show();
      jQuery( '.yith_wcn_badge_text_color_input' ).show();
    }else{
      jQuery( '.yith_wcn_badge_text_input' ).hide();
      jQuery( '.yith_wcn_badge_background_color_input' ).hide();
      jQuery( '.yith_wcn_badge_text_color_input' ).hide();
    }
  }else{
    jQuery( '.yith_wcn_note_label_input' ).hide();
    jQuery( '.yith_wcn_note_description_input' ).hide();
    jQuery( '.yith_wcn_field_type_input' ).hide();
    jQuery( '.yith_wcn_price_settings_input' ).hide();
    jQuery( '.yith_wcn_price_input' ).hide();
    jQuery( '.yith_wcn_free_characters_input' ).hide();
    jQuery( '.yith_wcn_show_badge_input' ).hide();
    jQuery( '.yith_wcn_badge_text_input' ).hide();
    jQuery( '.yith_wcn_badge_background_color_input' ).hide();
    jQuery( '.yith_wcn_badge_text_color_input' ).hide();
  }

  jQuery('#yith_wcn_enable_note').change(function() {
    if(this.checked) {
      jQuery( '.yith_wcn_note_label_input' ).show();
      jQuery( '.yith_wcn_note_description_input' ).show();
      jQuery( '.yith_wcn_field_type_input' ).show();
      jQuery( '.yith_wcn_price_settings_input' ).show();
      if ( jQuery( '#free' ).prop( 'checked' ) ) {
        jQuery( '.yith_wcn_price_input' ).hide();
        jQuery( '.yith_wcn_free_characters_input' ).hide();
      }else{
        jQuery( '.yith_wcn_price_input' ).show();
        jQuery( '.yith_wcn_free_characters_input' ).show();
      }
      jQuery( '.yith_wcn_show_badge_input' ).show();
      if ( jQuery( '#yith_wcn_show_badge' ).prop( 'checked' ) ) {
        jQuery( '.yith_wcn_badge_text_input' ).show();
        jQuery( '.yith_wcn_badge_background_color_input' ).show();
        jQuery( '.yith_wcn_badge_text_color_input' ).show();
      }else{
        jQuery( '.yith_wcn_badge_text_input' ).hide();
        jQuery( '.yith_wcn_badge_background_color_input' ).hide();
        jQuery( '.yith_wcn_badge_text_color_input' ).hide();
      }
    }else{
      jQuery( '.yith_wcn_note_label_input' ).hide();
      jQuery( '.yith_wcn_note_description_input' ).hide();
      jQuery( '.yith_wcn_field_type_input' ).hide();
      jQuery( '.yith_wcn_price_settings_input' ).hide();
      jQuery( '.yith_wcn_price_input' ).hide();
      jQuery( '.yith_wcn_free_characters_input' ).hide();
      jQuery( '.yith_wcn_show_badge_input' ).hide();
      jQuery( '.yith_wcn_badge_text_input' ).hide();
      jQuery( '.yith_wcn_badge_background_color_input' ).hide();
      jQuery( '.yith_wcn_badge_text_color_input' ).hide();
    }
  });

  jQuery('#yith_wcn_show_badge').change(function() {
    if(this.checked) {
      jQuery( '.yith_wcn_badge_text_input' ).show();
      jQuery( '.yith_wcn_badge_background_color_input' ).show();
      jQuery( '.yith_wcn_badge_text_color_input' ).show();
    }else{
      jQuery( '.yith_wcn_badge_text_input' ).hide();
      jQuery( '.yith_wcn_badge_background_color_input' ).hide();
      jQuery( '.yith_wcn_badge_text_color_input' ).hide();
    }
  });

  jQuery('input[type=radio][name=yith_wcn_price_settings]').change(function() {
    if(this.value == 'free') {
      jQuery( '.yith_wcn_price_input' ).hide();
      jQuery( '.yith_wcn_free_characters_input' ).hide();
    }else{
      jQuery( '.yith_wcn_price_input' ).show();
      jQuery( '.yith_wcn_free_characters_input' ).show();
    }
  });

});
