<?php
/**
 * YITH WCN Plugin Notes.
 *
 * @package plugin-wc
 */

if ( ! defined( 'YITH_WCN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_WCN_Admin' ) ) {

	/**
	 * YITH_WCN_Admin
	 */
	class YITH_WCN_Admin {

		/**
		 * Main Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_WCN_Admin Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'admin_enqueue_scripts', array( $this, 'yith_wcn_enqueue_scripts' ) );

			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_wcn_create_purchase_note_tab' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'yith_wcn_display_purchase_note_fields' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'yith_wcn_save_fields' ) );

			add_action( 'admin_menu', array( $this, 'yith_wcn_create_settings_submenu' ) );
			add_action( 'admin_init', array( $this, 'yith_wcn_init_settings_submenu' ) );
		}

		/**
		 * Enqueue backend scripts
		 */
		public function yith_wcn_enqueue_scripts() {
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'yith-wcn-admin-css', YITH_WCN_DIR_ASSETS_CSS_URL . '/yith-wcn-backend.css', array(), YITH_WCN_VERSION );
			wp_enqueue_script( 'yith-wcn-admin-js', YITH_WCN_DIR_ASSETS_JS_URL . '/yith-wcn-backend.js', array( 'wp-color-picker' ), YITH_WCN_VERSION );
		}

		/**
		 * Add the new tab to the $tabs array
		 *
		 * @param array $tabs tabs.
		 */
		public function yith_wcn_create_purchase_note_tab( $tabs ) {
			$tabs['purchase_note'] = array(
				'label'    => __( 'Purchase Note', 'yith-plugin-notes' ),
				'target'   => 'purchase_note_panel',
				'priority' => 80,
			);
			return $tabs;
		}

		/**
		 * Display fields for the new panel
		 */
		public function yith_wcn_display_purchase_note_fields() {

			$purchase_fields = array(
				array(
					'id'      => 'yith_wcn_enable_note',
					'title'   => esc_html__( 'Enable/Disable', 'yith-plugin-notes' ),
					'type'    => 'checkbox',
					'default' => '',
					'options' => '',
				),
				array(
					'id'      => 'yith_wcn_note_label',
					'title'   => esc_html__( 'Note Label', 'yith-plugin-notes' ),
					'type'    => 'text',
					'default' => 'Note',
					'options' => '',
				),
				array(
					'id'      => 'yith_wcn_note_description',
					'title'   => esc_html__( 'Note Description', 'yith-plugin-notes' ),
					'type'    => 'textarea',
					'default' => 'Description',
					'options' => '',
				),
				array(
					'id'      => 'yith_wcn_field_type',
					'title'   => esc_html__( 'Field Type', 'yith-plugin-notes' ),
					'type'    => 'radio',
					'default' => 'text',
					'options' => array(
						array(
							'name'  => 'text',
							'title' => esc_html__( 'Text', 'yith-plugin-notes' ),
						),
						array(
							'name'  => 'textarea',
							'title' => esc_html__( 'Textarea', 'yith-plugin-notes' ),
						),
					),
				),
				array(
					'id'      => 'yith_wcn_price_settings',
					'title'   => esc_html__( 'Price Settings', 'yith-plugin-notes' ),
					'type'    => 'radio',
					'default' => 'free',
					'options' => array(
						array(
							'name'  => 'free',
							'title' => esc_html__( 'Free', 'yith-plugin-notes' ),
						),
						array(
							'name'  => 'fixed_price',
							'title' => esc_html__( 'Fixed Price', 'yith-plugin-notes' ),
						),
						array(
							'name'  => 'price_per_character',
							'title' => esc_html__( 'Price per character', 'yith-plugin-notes' ),
						),
					),
				),
				array(
					'id'      => 'yith_wcn_price',
					'title'   => esc_html__( 'Price', 'yith-plugin-notes' ),
					'type'    => 'text',
					'default' => '0',
					'options' => '',
				),
				array(
					'id'      => 'yith_wcn_free_characters',
					'title'   => esc_html__( 'Free Characters', 'yith-plugin-notes' ),
					'type'    => 'number',
					'default' => 0,
					'options' => '',
				),
				array(
					'id'      => 'yith_wcn_show_badge',
					'title'   => esc_html__( 'Show Badge', 'yith-plugin-notes' ),
					'type'    => 'checkbox',
					'default' => '',
					'options' => '',
				),
				array(
					'id'      => 'yith_wcn_badge_text',
					'title'   => esc_html__( 'Badge Text', 'yith-plugin-notes' ),
					'type'    => 'text',
					'default' => 'Badge Text',
					'options' => '',
				),
				array(
					'id'      => 'yith_wcn_badge_background_color',
					'title'   => esc_html__( 'Badge Background Color', 'yith-plugin-notes' ),
					'type'    => 'color-pickup',
					'default' => '#007694',
					'options' => '',
				),
				array(
					'id'      => 'yith_wcn_badge_text_color',
					'title'   => esc_html__( 'Badge Text Color', 'yith-plugin-notes' ),
					'type'    => 'color-pickup',
					'default' => '#ffffff',
					'options' => '',
				),
			);

			echo '<div id="purchase_note_panel" class="panel woocommerce_options_panel">';
			echo '	<div class="options_group">';

			foreach ( $purchase_fields as $field ) {
				$this->yith_wcn_purchase_note_get_view( $field );
			}

			echo '</div>';
			echo '</div>';

		}

		public function yith_wcn_purchase_note_get_view( $array ) {
			extract( $array );
			global $post;

			yith_wcn_get_view(
				'/inputs/yith-wcn-'.$type.'.php',
				array(
					'id'         => $id,
					'title'      => $title,
					'product_id' => $post->ID,
					'default'    => $default,
					'options'	 => $options,
				)
			);
		}

		/**
		 * Save the custom fields
		 *
		 * @param int $post_id post id.
		 */
		public function yith_wcn_save_fields( $post_id ) {

			$product = wc_get_product( $post_id );

			$yith_wcn_enable_note = isset( $_POST['yith_wcn_enable_note'] ) ? $_POST['yith_wcn_enable_note'] : 'no';
			$product->update_meta_data( 'yith_wcn_enable_note', sanitize_text_field( $yith_wcn_enable_note ) );

			$yith_wcn_note_label = isset( $_POST['yith_wcn_note_label'] ) ? $_POST['yith_wcn_note_label'] : 'Note';
			$product->update_meta_data( 'yith_wcn_note_label', sanitize_text_field( $yith_wcn_note_label ) );

			$yith_wcn_note_description = isset( $_POST['yith_wcn_note_description'] ) ? $_POST['yith_wcn_note_description'] : '';
			$product->update_meta_data( 'yith_wcn_note_description', sanitize_text_field( $yith_wcn_note_description ) );

			$yith_wcn_field_type = isset( $_POST['yith_wcn_field_type'] ) ? $_POST['yith_wcn_field_type'] : 'text';
			$product->update_meta_data( 'yith_wcn_field_type', sanitize_text_field( $yith_wcn_field_type ) );

			$yith_wcn_price_settings = isset( $_POST['yith_wcn_price_settings'] ) ? $_POST['yith_wcn_price_settings'] : 'free';
			$product->update_meta_data( 'yith_wcn_price_settings', sanitize_text_field( $yith_wcn_price_settings ) );

			$yith_wcn_price = isset( $_POST['yith_wcn_price'] ) ? $_POST['yith_wcn_price'] : '';
			$product->update_meta_data( 'yith_wcn_price', sanitize_text_field( $yith_wcn_price ) );

			$yith_wcn_free_characters = isset( $_POST['yith_wcn_free_characters'] ) ? $_POST['yith_wcn_free_characters'] : 0;
			$product->update_meta_data( 'yith_wcn_free_characters', sanitize_text_field( $yith_wcn_free_characters ) );

			$yith_wcn_show_badge = isset( $_POST['yith_wcn_show_badge'] ) ? $_POST['yith_wcn_show_badge'] : 'no';
			$product->update_meta_data( 'yith_wcn_show_badge', sanitize_text_field( $yith_wcn_show_badge ) );

			$yith_wcn_badge_text = isset( $_POST['yith_wcn_badge_text'] ) ? $_POST['yith_wcn_badge_text'] : '';
			$product->update_meta_data( 'yith_wcn_badge_text', sanitize_text_field( $yith_wcn_badge_text ) );

			$yith_wcn_badge_background_color = isset( $_POST['yith_wcn_badge_background_color'] ) ? $_POST['yith_wcn_badge_background_color'] : '#007694';
			$product->update_meta_data( 'yith_wcn_badge_background_color', sanitize_text_field( $yith_wcn_badge_background_color ) );

			$yith_wcn_badge_text_color = isset( $_POST['yith_wcn_badge_text_color'] ) ? $_POST['yith_wcn_badge_text_color'] : '#ffffff';
			$product->update_meta_data( 'yith_wcn_badge_text_color', sanitize_text_field( $yith_wcn_badge_text_color ) );

			$product->save();

		}

		/**
		 * Create submenu in settings for Customization
		 */
		public function yith_wcn_create_settings_submenu() {
			add_submenu_page(
				'options-general.php',
				__( 'Product Purchase Note', 'yith-plugin-notes' ),
				__( 'Product Purchase Note', 'yith-plugin-notes' ),
				'manage_woocommerce',
				'purchase-note-products',
				array( $this, 'yith_wcn_options_view' ),
			);
		}

		/**
		 * Options View
		 */
		public function yith_wcn_options_view() {
			yith_wcn_get_view( '/admin/yith-wcn-plugin-options-panel.php', array() );
		}

		/**
		 * Init submenu in settings for Customization
		 */
		public function yith_wcn_init_settings_submenu() {
			$page_name    = 'wcn-options-page';
			$section_name = 'options_section';

			add_settings_section(
				$section_name,
				esc_html__( 'Note Box Data', 'yith-plugin-notes' ),
				'',
				$page_name
			);

			$settings_fields = array(
				array(
					'id'      => 'yith_wcn_padding',
					'title'   => esc_html__( 'Padding', 'yith-plugin-notes' ),
					'options' => array(
						array(
							'id'      => 'yith_wcn_padding_top',
							'name'    => 'yith_wcn_padding_top',
							'title'   => esc_html__( 'Top: ', 'yith-plugin-notes' ),
							'type'    => 'number',
							'default' => '20',
						),
						array(
							'id'      => 'yith_wcn_padding_bottom',
							'name'    => 'yith_wcn_padding_bottom',
							'title'   => esc_html__( 'Bottom: ', 'yith-plugin-notes' ),
							'type'    => 'number',
							'default' => '25',
						),
						array(
							'id'      => 'yith_wcn_padding_left',
							'name'    => 'yith_wcn_padding_left',
							'title'   => esc_html__( 'Left: ', 'yith-plugin-notes' ),
							'type'    => 'number',
							'default' => '25',
						),
						array(
							'id'      => 'yith_wcn_padding_right',
							'name'    => 'yith_wcn_padding_right',
							'title'   => esc_html__( 'Right: ', 'yith-plugin-notes' ),
							'type'    => 'number',
							'default' => '25',
						),
					),
				),
				array(
					'id'      => 'yith_wcn_border',
					'title'   => esc_html__( 'Border', 'yith-plugin-notes' ),
					'options' => array(
						array(
							'id'      => 'yith_wcn_border_weight',
							'name'    => 'yith_wcn_border_weight',
							'title'   => esc_html__( 'Weight: ', 'yith-plugin-notes' ),
							'type'    => 'number',
							'default' => '1',
						),
						array(
							'id'      => 'yith_wcn_border_style',
							'title'   => esc_html__( 'Style: ', 'yith-plugin-notes' ),
							'type'    => 'select',
							'default' => 'solid',
							'options' => array(
								array(
									'value' => 'solid',
									'name'  => esc_html__( 'Solid', 'yith-plugin-notes' ),
								),
								array(
									'value' => 'dotted',
									'name'  => esc_html__( 'Dotted', 'yith-plugin-notes' ),
								),
								array(
									'value' => 'dashed',
									'name'  => esc_html__( 'Dashed', 'yith-plugin-notes' ),
								),
								array(
									'value' => 'double',
									'name'  => esc_html__( 'Double', 'yith-plugin-notes' ),
								),
								array(
									'value' => 'groove',
									'name'  => esc_html__( 'Groove', 'yith-plugin-notes' ),
								),
								array(
									'value' => 'ridge',
									'name'  => esc_html__( 'Ridge', 'yith-plugin-notes' ),
								),
								array(
									'value' => 'inset',
									'name'  => esc_html__( 'Inset', 'yith-plugin-notes' ),
								),
								array(
									'value' => 'outset',
									'name'  => esc_html__( 'Outset', 'yith-plugin-notes' ),
								),
								array(
									'value' => 'none',
									'name'  => esc_html__( 'None', 'yith-plugin-notes' ),
								),
							),
						),
						array(
							'id'      => 'yith_wcn_border_color',
							'title'   => esc_html__( 'Color: ', 'yith-plugin-notes' ),
							'type'    => 'color-pickup',
							'default' => '#d8d8d8',
						),
						array(
							'id'      => 'yith_wcn_border_radius',
							'title'   => esc_html__( 'Radius: ', 'yith-plugin-notes' ),
							'type'    => 'number',
							'default' => '7',
						),
					),

				),
				array(
					'id'      => 'yith_wcn_badge_position_shop',
					'title'   => esc_html__( 'Badge Position in Shop', 'yith-plugin-notes' ),
					'options' => array(
						array(
							'id'      => 'yith_wcn_badge_position_shop',
							'type'    => 'radio',
							'default' => 'top-left',
							'options' => array(
								array(
									'name'  => 'top-left',
									'title' => esc_html__( 'Top Left', 'yith-plugin-notes' ),
								),
								array(
									'name'  => 'top-right',
									'title' => esc_html__( 'Top Right', 'yith-plugin-notes' ),
								),
							),
						),
					),
				),
				array(
					'id'      => 'yith_wcn_badge_position_product',
					'title'   => esc_html__( 'Badge Position in Product', 'yith-plugin-notes' ),
					'options' => array(
						array(
							'id'      => 'yith_wcn_badge_position_product',
							'type'    => 'radio',
							'default' => 'top-left',
							'options' => array(
								array(
									'name'  => 'top-left',
									'title' => esc_html__( 'Top Left', 'yith-plugin-notes' ),
								),
								array(
									'name'  => 'top-right',
									'title' => esc_html__( 'Top Right', 'yith-plugin-notes' ),
								),
							),
						),
					),
				),
			);

			foreach ( $settings_fields as $field ) {
				add_settings_field(
					$field['id'],
					$field['title'],
					array( $this, 'yith_wcn_purchase_note_submenu_fields' ),
					$page_name,
					$section_name,
					array(
						'label_for' => $field['id'],
						'options'   => $field['options'],
					)
				);

				foreach ( $field['options'] as $option ) {
					register_setting( $page_name, $option['id'] );
				}
			}

		}

		/**
		 * Get View for Settings
		 *
		 * @param array $array contains file name value.
		 */
		public function yith_wcn_purchase_note_submenu_fields( $array ) {
			extract( $array );

			foreach ( $options as $option ){
				yith_wcn_get_view(
					'/inputs/yith-wcn-' . $option['type'] . '.php',
					array(
						'id'      => $option['id'],
						'default' => $option['default'],
						'options' => $option['options'],
						'title'   => $option['title'],
					)
				);
			}
		}

	}

}
