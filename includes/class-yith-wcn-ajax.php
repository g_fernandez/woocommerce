<?php
/**
 * YITH WCN Plugin Notes.
 *
 * @package plugin-wc
 */

if ( ! defined( 'YITH_WCN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_WCN_Ajax' ) ) {

	/**
	 * YITH_WCN_Admin
	 */
	class YITH_WCN_Ajax {

		/**
		 * Main Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_WCN_Ajax Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_wcn_enqueue_script_ajax' ) );
			add_action( 'wp_ajax_change_note_price', array( $this, 'change_note_price' ) );
			add_action( 'wp_ajax_nopriv_change_note_price', array( $this, 'change_note_price' ) );
		}

		/**
		 * Ajax Scripts
		 */
		public function yith_wcn_enqueue_script_ajax() {
			wp_register_script( 'ajax_script', YITH_WCN_DIR_ASSETS_JS_URL . '/yith-wcn-ajax.js', array( 'jquery' ), YITH_WCN_VERSION, false );
			wp_localize_script( 'ajax_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'ajax_script' );

		}

		/**
		 * Change Note Price
		 */
		public function change_note_price() {

			$note_price_settings = get_post_meta( $_POST['product_id'], 'yith_wcn_price_settings', true );
			$note_price          = get_post_meta( $_POST['product_id'], 'yith_wcn_price', true );
			$free_characters     = get_post_meta( $_POST['product_id'], 'yith_wcn_free_characters', true );
			$price = 0;

			if ( 'fixed_price' === $note_price_settings ) {
				if ( strlen( $_POST['input_val'] ) > (int) $free_characters ) {
					$price = (float) $note_price;
				}
			} elseif ( 'price_per_character' === $note_price_settings ) {
				if ( strlen( $_POST['input_val']) > (float) $free_characters ) {
					$characters = (float) strlen( $_POST['input_val'] );
					$price      = (float) $note_price * $characters;
				}
			}

			$output = array(
				'price'  => $price,
				'symbol' => get_woocommerce_currency_symbol(),
				);

			echo wp_json_encode( $output );
			die();
		}

	}
}
