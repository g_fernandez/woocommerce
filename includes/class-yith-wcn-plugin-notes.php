<?php
/**
 * YITH WCN Plugin Notes.
 *
 * @package plugin-wc
 */

if ( ! defined( 'YITH_WCN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_WCN_Plugin_Notes' ) ) {
	/**
	 * YITH_WCN_Plugin_Notes
	 */
	class YITH_WCN_Plugin_Notes {

		/**
		 * Plugin Notes instance.
		 *
		 * @var YITH_WCN_Plugin_Notes
		 */
		private static $instance;

		/**
		 * Admin class instance.
		 *
		 * @var YITH_WCN_Admin
		 */
		public $admin = null;

		/**
		 * Frontend class instance.
		 *
		 * @var YITH_WCN_Frontend
		 */
		public $frontend = null;

		/**
		 * Ajax class instance.
		 *
		 * @var YITH_WCN_Ajax
		 */
		public $ajax = null;

		/**
		 * Get_instance
		 *
		 * @return YITH_WCN_Plugin_Notes Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_wcn_require_class',
				array(
					'common' => array(
						'includes/functions.php',
						'includes/class-yith-wcn-ajax.php',
					),
					'admin'  => array(
						'includes/class-yith-wcn-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-wcn-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			$this->init();

		}

		/**
		 * _require
		 *
		 * @param mixed $main_classes Main.
		 * @return void
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_WCN_DIR_PATH . $class ) ) {
						require_once( YITH_WCN_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			$this->ajax = YITH_WCN_Ajax::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {

			if ( is_admin() ) {
				$this->admin = YITH_WCN_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_WCN_Frontend::get_instance();
			}

		}

	}

}

if ( ! function_exists( 'yith_wcn_plugin_notes' ) ) {
	/**
	 * Yith_wcn_plugin_notes
	 *
	 * @return YITH_WCN_Plugin_Notes Instance.
	 */
	function yith_wcn_plugin_notes() {
		return YITH_WCN_Plugin_Notes::get_instance();
	}
}
