<?php
/**
 * YITH WCN Plugin Notes.
 *
 * @package plugin-wc
 */

if ( ! function_exists( 'yith_wcn_get_view' ) ) {
	/**
	 * Include views
	 *
	 * @param  mixed $file_name Name of file.
	 * @param  mixed $args Arguments.
	 * @return void
	 */
	function yith_wcn_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_WCN_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
