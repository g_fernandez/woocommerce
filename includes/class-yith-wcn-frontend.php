<?php
/**
 * YITH WCN Plugin Notes.
 *
 * @package plugin-wc
 */

if ( ! defined( 'YITH_WCN_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_WCN_Frontend' ) ) {

	/**
	 * YITH_WCN_Frontend
	 */
	class YITH_WCN_Frontend {

		/**
		 * YITH_WCN_Frontend instance.
		 *
		 * @var YITH_WCN_Frontend
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_WCN_Frontend Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_wcn_enqueue_scripts' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_wcn_add_purchase_note_to_product_page' ) );

			// Badges.
			add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'yith_wcn_add_badge_to_shop_page_images' ) );
			add_action( 'woocommerce_product_thumbnails', array( $this, 'yith_wcn_add_badge_to_product_page_images' ) );

			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'yith_wcn_add_cart_item_data' ), 10, 2 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'yith_wcn_add_custom_price' ), 10, 1 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_wcn_product_add_on_display_cart' ), 10, 2 );
			add_action( 'woocommerce_add_order_item_meta', array( $this, 'yith_wcn_product_add_on_order_item_meta' ), 10, 2 );
			add_filter( 'woocommerce_order_item_product', array( $this, 'yith_wcn_product_add_on_display_order' ), 10, 2 );
		}

		/**
		 * Enqueue frontend scripts
		 */
		public function yith_wcn_enqueue_scripts() {
			wp_enqueue_style( 'yith-wcn-wp-css', YITH_WCN_DIR_ASSETS_CSS_URL . '/yith-wcn-frontend.css', array(), YITH_WCN_VERSION );
			$this->yith_wcn_custom_style(); // Enqueue all customs styles .
		}

		/**
		 * Enqueue frontend custom style scripts
		 */
		public function yith_wcn_custom_style() {
			global $post;
			$background_color = get_post_meta( $post->ID, 'yith_wcn_badge_background_color', true );
			$text_color       = get_post_meta( $post->ID, 'yith_wcn_badge_text_color', true );


			$border_weight    = get_option( 'yith_wcn_border_weight', '1' );
			$border_style     = get_option( 'yith_wcn_border_style', 'solid' );
			$border_color     = get_option( 'yith_wcn_border_color', '#d8d8d8' );
			$border_radius    = get_option( 'yith_wcn_border_radius', '7' );
			$padding_top      = get_option( 'yith_wcn_padding_top', '20' );
			$padding_right    = get_option( 'yith_wcn_padding_right', '25' );
			$padding_bottom   = get_option( 'yith_wcn_padding_bottom', '25' );
			$padding_left     = get_option( 'yith_wcn_padding_left', '25' );
			$position_product = get_option( 'yith_wcn_badge_position_product', 'top-left' );
			$position_shop    = get_option( 'yith_wcn_badge_position_shop', 'top-left' );

			$custom_css = '
			.note-container{
				border-width: '.$border_weight.'px;
				border-style: '.$border_style.';
				border-color: '.$border_color.';
				border-radius: '.$border_radius.'px;
				padding-top: '.$padding_top.'px;
				padding-bottom: '.$padding_bottom.'px;
				padding-left: '.$padding_left.'px;
				padding-right: '.$padding_right.'px;
			}';
			wp_add_inline_style( 'yith-wcn-wp-css', $custom_css );

			if ( 'top-left' === $position_product ) {
				$custom_css = '
				.soldout_product {
					left: 3px;
					right: auto;
				}';
			} else {
				$custom_css = '
				.soldout_product {
					right: 3px;
					left: auto;
				}';
			}
			wp_add_inline_style( 'yith-wcn-wp-css', $custom_css );

			if ( 'top-left' === $position_shop ) {
				$custom_css = '
				.soldout_shop {
					left: 3px;
					right: auto;
				}';
			} else {
				$custom_css = '
				.soldout_shop {
					right: 3px;
					left: auto;
				}';
			}
			wp_add_inline_style( 'yith-wcn-wp-css', $custom_css );

			$custom_css2 = '
			.badge_product_' . $post->ID . '{
		  		background-color: ' . $background_color . ';
		  		color: ' . $text_color . ';
			}';
			wp_add_inline_style( 'yith-wcn-wp-css', $custom_css2 );

		}

		/**
		 * Add purchase note to the product page
		 *
		 * @return void
		 */
		public function yith_wcn_add_purchase_note_to_product_page() {
			global $product;
			$option = get_post_meta( $product->get_id(), 'yith_wcn_enable_note', true );

			$args = array(
				'product_id' => $product->get_id(),
			);

			if ( 'yes' === $option ) {
				wc_get_template( '/product-page/yith-wcn-purchase-note.php', $args, '', trailingslashit( YITH_WCN_DIR_TEMPLATES_PATH ) );
			}
		}

		/**
		 * Add badge text in shop page images
		 *
		 * @return void
		 */
		public function yith_wcn_add_badge_to_shop_page_images() {
			global $product;
			$option         = get_post_meta( $product->get_id(), 'yith_wcn_show_badge', true );
			$badge_position = get_option( 'yith_wcn_badge_position_shop', '' );

			$args = array(
				'product_id'          => $product->get_id(),
				'badge_position_shop' => $badge_position,
			);

			if ( 'yes' === $option ) {
				wc_get_template( '/product-page/yith-wcn-badge-shop.php', $args, '', trailingslashit( YITH_WCN_DIR_TEMPLATES_PATH ) );
			}
		}

		/**
		 * Add badge text in product page images
		 *
		 * @return void
		 */
		public function yith_wcn_add_badge_to_product_page_images() {
			global $product;
			$option         = get_post_meta( $product->get_id(), 'yith_wcn_show_badge', true );
			$badge_position = get_option( 'yith_wcn_badge_position_product', '' );
			$args           = array(
				'product_id'             => $product->get_id(),
				'badge_position_product' => $badge_position,
			);

			if ( 'yes' === $option ) {
				wc_get_template( '/product-page/yith-wcn-badge-product.php', $args, '', trailingslashit( YITH_WCN_DIR_TEMPLATES_PATH ) );
			}
		}

		/**
		 * Save purchase note input value into cart item data.
		 *
		 * @param Array $cart_item_data Product item.
		 * @param int   $product_id Product ID.
		 */
		public function yith_wcn_add_cart_item_data( $cart_item_data, $product_id ) {
			if ( isset( $_POST['yith_wcn_purchase_note_text_field'] ) ) {
				$cart_item_data['yith_wcn_purchase_note_text_field'] = sanitize_text_field( $_POST['yith_wcn_purchase_note_text_field'] );

				$note_price                            = $this->yith_wcn_calculate_price( $_POST['yith_wcn_purchase_note_text_field'], $product_id );
				$cart_item_data['yith_wcn_note_price'] = $note_price;
			}
			return $cart_item_data;
		}

		/**
		 * Calculate total price with the note.
		 *
		 * @param String $text_field Text Field.
		 * @param int    $product_id Product ID.
		 */
		public function yith_wcn_calculate_price( $text_field, $product_id ) {
			$product = wc_get_product( $product_id );
			$price   = (float) $product->get_price();

			$note_price_settings = get_post_meta( $product_id, 'yith_wcn_price_settings', true );
			$note_price          = get_post_meta( $product_id, 'yith_wcn_price', true );
			$free_characters     = get_post_meta( $product_id, 'yith_wcn_free_characters', true );

			if ( 'fixed_price' === $note_price_settings ) {
				if ( strlen( $text_field ) > (int) $free_characters ) {
					$price += (float) $note_price;
				}
			} elseif ( 'price_per_character' === $note_price_settings ) {
				if ( strlen( $text_field ) > (float) $free_characters ) {
					$characters = (float) strlen( $text_field );
					$price     += (float) $note_price * $characters;
				}
			}
			return $price;
		}

		/**
		 * Set new price to products with custom note.
		 *
		 * @param Array $cart Cart.
		 */
		public function yith_wcn_add_custom_price( $cart ) {
			foreach ( $cart->get_cart() as $hash => $value ) {
				if ( isset( $value['yith_wcn_note_price'] ) ) {
					$value['data']->set_price( (float) $value['yith_wcn_note_price'] );
				}
			}
		}

		/**
		 * Display purchase note on cart.
		 *
		 * @param Array $item_data item data.
		 * @param Array $cart_item cart item.
		 */
		public function yith_wcn_product_add_on_display_cart( $item_data, $cart_item ) {
			if ( isset( $cart_item['yith_wcn_purchase_note_text_field'] ) ) {
				$item_data[] = array(
					'name'  => 'Purchase Note',
					'value' => sanitize_text_field( $cart_item['yith_wcn_purchase_note_text_field'] ),
				);
			}

			return $item_data;
		}

		/**
		 * Product add on order item meta
		 *
		 * @param Int   $item_id item ID.
		 * @param Array $values Values.
		 */
		public function yith_wcn_product_add_on_order_item_meta( $item_id, $values ) {
			if ( ! empty( $values['yith_wcn_purchase_note_text_field'] ) ) {
				wc_add_order_item_meta( $item_id, 'Purchase Note', $values['yith_wcn_purchase_note_text_field'], true );
			}
		}

		/**
		 * Product add on display order
		 *
		 * @param Array $product item ID.
		 * @param Array $item Item.
		 */
		public function yith_wcn_product_add_on_display_order( $product, $item ) {
			if ( isset( $item['yith_wcn_purchase_note_text_field'] ) ) {
				$product['yith_wcn_purchase_note_text_field'] = $item['yith_wcn_purchase_note_text_field'];
			}
			return $product;
		}

	}

}
