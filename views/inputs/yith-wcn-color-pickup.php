<?php
if ( isset( $product_id ) ){
	$value = get_post_meta( $product_id, $id, true );
} else {
	$value = get_option( $id, '' );
}
?>
<?php
if ( isset( $product_id ) ) {
	?>
<p class="form-field <?php echo esc_attr( $id . '_input' ); ?>">
	<?php
}
?>
	<label for="<?php echo $id ?>"><?php echo $title ?></label>
	<input
		type="text"
		value="<?php echo '' !== $value ? esc_attr( $value ) : $default; ?>"
		class="my-color-field"
		name="<?php echo $id ?>"
		id="<?php echo $id ?>">
	<?php
	if ( isset( $product_id ) ) {
		?>
</p>
		<?php
	}
	?>
