<?php
if ( isset( $product_id ) ) {
	$value = intval( get_post_meta( $product_id, $id, true ) );
} else {
	$value = get_option( $id, '' );
}
?>
<?php
if ( isset( $product_id ) ) {
	?>
<p class="form-field <?php echo esc_attr( $id . '_input' ); ?>">
	<?php
}
?>
	<label for="<?php echo esc_attr( $id ); ?>"><?php echo esc_attr( $title ); ?></label>
	<input
		type="number"
		id="<?php echo esc_attr( $id ); ?>"
		name="<?php echo esc_attr( $id ); ?>"
		value="<?php echo '' !== $value ? esc_attr( $value ) : esc_attr( $default ); ?>">
	<?php
	if ( isset( $product_id ) ) {
		?>
</p>
		<?php
	}
	?>
