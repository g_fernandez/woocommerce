<?php
if ( isset( $product_id ) ){
	$checked_value = get_post_meta( $product_id, $id, true );
} else {
	$checked_value = get_option( $id, '' );
}
?>
<?php
if ( isset( $product_id ) ) {
	?>
<p class="form-field <?php echo esc_attr( $id . '_input' ); ?>">
	<?php
}
?>
	<label for="<?php echo $id ?>"><?php echo $title ?></label>


	<label class="switch">

		<input
			type="checkbox"
			name="<?php echo $id ?>"
			value='yes'
			id="<?php echo $id ?>"
			<?php checked( $checked_value, 'yes' ); ?>
		>
		<span class="slider round"></span>
	</label>


	<?php
	if ( isset( $product_id ) ) {
		?>
</p>
		<?php
	}
	?>

