<?php
if ( isset( $product_id ) ){
	$value = get_post_meta( $product_id, $id, true );
} else {
	$value = get_option( $id, '' );
}
?>
<?php
if ( isset( $product_id ) ) {
	?>
<p class="form-field <?php echo esc_attr( $id . '_input' ); ?>">
	<?php
}
?>
	<label for="<?php echo $id ?>"><?php echo $title ?></label>
	<textarea id="<?php echo $id ?>" name="<?php echo $id ?>" rows="4" cols="50"><?php echo '' !== $value ? esc_attr( $value ) : $default; ?></textarea>
	<?php
	if ( isset( $product_id ) ) {
		?>
</p>
		<?php
	}
	?>
