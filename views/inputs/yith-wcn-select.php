<?php
if ( isset( $product_id ) ){
	$value = get_post_meta( $product_id, $id, true );
} else {
	$value = get_option( $id, '' );
}
?>
<label for="<?php echo esc_attr( $id ); ?>" id="<?php echo esc_attr( $id ); ?>"><?php echo esc_attr( $title ); ?></label>
<select name="<?php echo esc_attr( $id ); ?>">
	<?php
	foreach ( $options as $option ) {
		?>
	<option value="<?php echo esc_attr( $option['value'] ); ?>" <?php echo $option['value'] === $value ? 'selected' : ''; ?>><?php echo esc_attr( $option['name'] ); ?></option>
		<?php
	}
	?>
</select>
