<?php
if ( isset( $product_id ) ){
	$checked_value = get_post_meta( $product_id, $id, true );
} else {
	$checked_value = get_option( $id, '' );
}
?>
<fieldset class="form-field <?php echo esc_attr( $id . '_input' ); ?>">
	<?php
	if ( isset( $title ) ){
		?>
	<legend><?php echo esc_attr( $title ); ?></legend>
	<?php
	}
	?>
	<ul class="wc-radios">
		<?php
		foreach ( $options as $option ) {
			?>
		<li>
			<label>
				<input type="radio" id="<?php echo esc_attr( $option['name'] ); ?>" name="<?php echo esc_attr( $id ); ?>" value="<?php echo esc_attr( $option['name'] ); ?>" <?php checked( $checked_value, $option['name'] ); ?>>
				<?php echo esc_attr( $option['title'] ); ?>
			</label>
		</li>
			<?php
		}
		?>
	</ul>
</fieldset>
