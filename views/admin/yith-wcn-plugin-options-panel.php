<?php

?>

<div class="wrap">
	<h1><?php esc_html_e( 'Note Box Customization Panel', 'yith-plugin-notes' ); ?></h1>

	<form method="post" action="options.php">
		<?php
			settings_fields( 'wcn-options-page' );
			do_settings_sections( 'wcn-options-page' );
			submit_button();
		?>

	</form>
</div>
